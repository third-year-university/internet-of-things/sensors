import pickle
import time

import serial

ser = serial.Serial("/dev/ttyACM0", timeout=1)

values = {}

time.sleep(5)

while ser.in_waiting >= 4:
    data = ser.read(4)
    dst = data[0] * 255 + data[1]
    value = data[2] * 255 + data[3]
    if dst not in values:
        values[dst] = [0, 0]
    values[dst][0] += value
    values[dst][1] += 1
    print(dst, values)
    time.sleep(0.25)

f = open('data2.txt', 'wb')

pickle.dump(values, f)
