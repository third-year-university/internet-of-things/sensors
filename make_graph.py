import pickle
from matplotlib import pyplot as plt

f = open('data2.txt', 'rb')

data = pickle.load(f)

data = dict(sorted(data.items()))

distances = []
values = []

for key in data.keys():
    if 10 <= key <= 80:
        distances.append(key)
        values.append(int(data[key][0] / data[key][1]))

x = [i for i in range(10, 81)]
y = [67.8245 + 4165.4686 / xi for xi in x]


plt.scatter(distances, values)
plt.plot(x, y)
plt.show()